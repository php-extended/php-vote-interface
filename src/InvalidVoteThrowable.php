<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Throwable;

/**
 * InvalidVoteThrowable interface file.
 * 
 * This is thrown when a vote is rejected.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface InvalidVoteThrowable extends Throwable
{
	
	/**
	 * Gets the citizen that provided the invalid vote.
	 * 
	 * @return CitizenInterface<T>
	 */
	public function getCitizen() : CitizenInterface;
	
	/**
	 * Gets the vote that is invalid.
	 * 
	 * @return VoteInterface<T>
	 */
	public function getVote() : VoteInterface;
	
}
