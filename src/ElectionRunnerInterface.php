<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionRunnerInterface interface file.
 * 
 * This class processes the elections by giving a result for an election with
 * specific voting system and a pool of citizens.
 * 
 * @author Anastaszor
 */
interface ElectionRunnerInterface extends Stringable
{
	
	/**
	 * Runs a new election with the given voting method, the added biases, and
	 * the pool of citizens that may vote for that election.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param VotingMethodInterface $votingMethod
	 * @param array<integer, BiasInterface<T>> $biases
	 * @param array<integer, CitizenInterface<T>> $citizens
	 * @return ElectionResultInterface<T> if all is well
	 * @throws InvalidCandidateThrowable<T> if a citizen tries to register an 
	 *                                   invalid candidate
	 * @throws InvalidVoteThrowable<T> if a citizen tries to register an 
	 *                              invalid vote
	 * @throws UnsolvableSituationThrowable<T> if the given voting system cannot
	 *                                      rank the candidates given by the votes of the citizens
	 */
	public function runElection(VotingMethodInterface $votingMethod, array $biases, array $citizens) : ElectionResultInterface;
	
}
