<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * CitizenFactoryInterface interface file.
 * 
 * This interface specifies an object to create citizen objects.
 * 
 * @author Anastaszor
 */
interface CitizenFactoryInterface extends Stringable
{
	
	/**
	 * Gets the id of this citizen.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param string $identifier
	 * @param array<integer, CandidateInterface<T>> $candidates
	 * @return CitizenInterface<T>
	 */
	public function createCitizen(string $identifier, array $candidates) : CitizenInterface;
	
}
