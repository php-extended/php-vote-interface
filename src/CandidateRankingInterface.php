<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * CandidateRanking interface file.
 * 
 * This interface represents a ranking for a given candidate.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface CandidateRankingInterface extends Stringable
{
	
	/**
	 * Gets the identifier of the candidate ranking given by the citizen.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the iterator with the candidates given by the election that ranks
	 * equally at this ranking.
	 * 
	 * @return array<integer, CandidateInterface<T>>
	 */
	public function getCandidates() : array;
	
	/**
	 * Gets the score of this candidate ranking for the citizen.
	 * 
	 * @return ScoreInterface
	 */
	public function getScore() : ScoreInterface;
	
}
