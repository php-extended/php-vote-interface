<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * CandidateRankingFactoryInterface interface file.
 * 
 * This class specifies an object to create candidate ranking objects.
 * 
 * @author Anastaszor
 */
interface CandidateRankingFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new candidate ranking from the vote.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param string $identifier
	 * @param array<integer, CandidateInterface<T>> $candidates
	 * @param ScoreInterface $score
	 * @return CandidateRankingInterface<T>
	 */
	public function createCandidateRanking(string $identifier, array $candidates, ScoreInterface $score) : CandidateRankingInterface;
	
}
