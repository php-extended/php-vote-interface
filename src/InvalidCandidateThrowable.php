<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Throwable;

/**
 * InvalidCandidateThrowable class file.
 * 
 * This is thrown when a provided candidate is rejected.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface InvalidCandidateThrowable extends Throwable
{
	
	/**
	 * Gets the citizen that provided the invalid candidate.
	 * 
	 * @return CitizenInterface<T>
	 */
	public function getCitizen() : CitizenInterface;
	
	/**
	 * Gets the invalid candidate.
	 * 
	 * @return CandidateInterface<T>
	 */
	public function getCandidate() : CandidateInterface;
	
}
