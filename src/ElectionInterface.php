<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionInterface interface file.
 * 
 * This interface represents the object that manages the ordering of the candidates
 * according to the votes of that were given and the biases that are processed
 * for each vote.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface ElectionInterface extends Stringable
{
	
	/**
	 * Gets the identifier of the election.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Registers a candidate for this election.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param CandidateInterface<T> $candidate
	 * @return string the identifier given to that candidate by the election
	 * @throws InvalidCandidateThrowable<T> if the candidate is inappropriate
	 */
	public function registerCandidate(CitizenInterface $citizen, CandidateInterface $candidate) : string;
	
	/**
	 * Gets the current list of candidates.
	 * 
	 * @return array<integer, CandidateInterface<T>>
	 */
	public function getCandidates() : array;
	
	/**
	 * Registers a vote for this election.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param VoteInterface<T> $vote
	 * @return string the identifier given to that vote by the election
	 * @throws InvalidVoteThrowable<T> if the vote is inappropriate
	 */
	public function registerVote(CitizenInterface $citizen, VoteInterface $vote) : string;
	
	/**
	 * Registers a bias for this election.
	 * 
	 * @param BiasInterface<T> $bias
	 * @return boolean whether the bias is correctly registered
	 */
	public function registerBias(BiasInterface $bias) : bool;
	
	/**
	 * Gets the results of the voting system : an list of candidates, ordered
	 * by the votes on which this election runned.
	 * 
	 * @param VotingMethodInterface $votingMethod
	 * @return ElectionResultInterface<T>
	 * @throws UnsolvableSituationThrowable<T> if given voting method cannot rank
	 *                                      the candidates giving the current votes
	 */
	public function getResult(VotingMethodInterface $votingMethod) : ElectionResultInterface;
	
}
