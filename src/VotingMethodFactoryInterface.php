<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * VotingMethodFactoryInterface interface file.
 * 
 * This class specifies an object to create voting method objects.
 * 
 * @author Anastaszor
 */
interface VotingMethodFactoryInterface extends Stringable
{
	
	/**
	 * Creates a VotingMethod over boolean values.
	 *
	 * @return VotingMethodInterface
	 */
	public function createVotingMethod() : VotingMethodInterface;
	
}
