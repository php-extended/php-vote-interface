<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Throwable;

/**
 * UnsolvableSituationThrowable interface file.
 * 
 * This is thrown when a voting method cannot sort a result set.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface UnsolvableSituationThrowable extends Throwable
{
	
	/**
	 * Gets the voting method that is unable to solve the vote.
	 * 
	 * @return VotingMethodInterface
	 */
	public function getMethod() : VotingMethodInterface;
	
	/**
	 * Gets the set of results that could not be sorted.
	 * 
	 * @return array<integer, CandidateResultInterface<T>>
	 */
	public function getResults() : array;
	
}
