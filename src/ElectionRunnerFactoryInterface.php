<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionRunnerFactoryInterface interface file.
 * 
 * This class specifies an object to create election runner objects.
 * 
 * @author Anastaszor
 */
interface ElectionRunnerFactoryInterface extends Stringable
{
	
	/**
	 * Creates a ElectionRunner over boolean values.
	 *
	 * @return ElectionRunnerInterface
	 */
	public function createElectionRunner() : ElectionRunnerInterface;
	
}
