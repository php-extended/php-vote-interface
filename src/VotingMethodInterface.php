<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * VotingMethodInterface interface file.
 * 
 * This class is the resolver of the data gathered by the election. It is the
 * engine that converts the votes into an ordered list of candidates.
 * 
 * @author Anastaszor
 */
interface VotingMethodInterface extends Stringable
{
	
	/**
	 * Processes the current state of the election and gives a result (i.e. an
	 * ordering of candidates).
	 * 
	 * @template T of boolean|integer|float|string
	 * @param ElectionInterface<T> $election
	 * @param array<integer, CandidateInterface<T>> $candidates
	 * @param array<integer, VoteInterface<T>> $votes
	 * @return ElectionResultInterface<T>
	 * @throws UnsolvableSituationThrowable if this voting method cannot rank
	 *                                      the candidates giving the current votes
	 */
	public function resolve(ElectionInterface $election, array $candidates, array $votes) : ElectionResultInterface;
	
}
