<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * CandidateFactoryInterface interface file.
 * 
 * This class specifies an object to create candidate objects.
 * 
 * @author Anastaszor
 */
interface CandidateFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new candidate that votes for boolean values.
	 * 
	 * @param string $identifier
	 * @param boolean $value
	 * @return CandidateInterface<boolean>
	 */
	public function createBooleanCandidate(string $identifier, ?bool $value) : CandidateInterface;
	
	/**
	 * Creates a new candidate that votes for integer values.
	 * 
	 * @param string $identifier
	 * @param int $value
	 * @return CandidateInterface<integer>
	 */
	public function createIntegerCandidate(string $identifier, ?int $value) : CandidateInterface;
	
	/**
	 * Creates a new candidate that votes for float values.
	 * 
	 * @param string $identifier
	 * @param float $value
	 * @return CandidateInterface<float>
	 */
	public function createFloatCandidate(string $identifier, ?float $value) : CandidateInterface;
	
	/**
	 * Creates a new candidate that votes for string values.
	 * 
	 * @param string $identifier
	 * @param string $value
	 * @return CandidateInterface<string>
	 */
	public function createStringCandidate(string $identifier, ?string $value) : CandidateInterface;
	
}
