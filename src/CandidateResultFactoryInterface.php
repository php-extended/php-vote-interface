<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * CandidateResultFactoryInterface interface file.
 * 
 * This class specifies an object to create candidate result objects.
 * 
 * @author Anastaszor
 */
interface CandidateResultFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new candidate result to vote.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param string $identifier
	 * @param CandidateInterface<T> $candidate
	 * @param integer $points
	 * @param ScoreInterface $score
	 * @return CandidateResultInterface<T>
	 */
	public function createCandidateResult(string $identifier, CandidateInterface $candidate, int $points, ScoreInterface $score) : CandidateResultInterface;
	
}
