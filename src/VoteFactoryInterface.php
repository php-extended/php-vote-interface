<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * VoteFactoryInterface interface file.
 * 
 * This class specifies an object to create vote objects.
 * 
 * @author Anastaszor
 */
interface VoteFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new vote for an election.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param string $identifier
	 * @param array<integer, CandidateRankingInterface<T>> $rankings
	 * @param ScoreInterface $score
	 * @return VoteInterface<T>
	 */
	public function createVote(string $identifier, array $rankings, ScoreInterface $score) : VoteInterface;
	
}
