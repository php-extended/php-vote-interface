<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * CitizenInterface interface file.
 * 
 * This interface represents an object which provides candidates and votes for
 * the elections.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface CitizenInterface extends Stringable
{
	
	/**
	 * Gets the id of this citizen.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the candidates this citizen wants to see in the election.
	 * 
	 * @param ElectionInterface<T> $election
	 * @return array<integer, CandidateInterface<T>>
	 */
	public function proposeCandidates(ElectionInterface $election) : array;
	
	/**
	 * Review the given candidate that candidates for the given election. This
	 * precedes the voting process.
	 * 
	 * @param ElectionInterface<T> $election
	 * @param CandidateInterface<T> $candidate
	 * @return boolean whether the review was successful
	 */
	public function reviewCandidate(ElectionInterface $election, CandidateInterface $candidate) : bool;
	
	/**
	 * Votes within the given election.
	 * 
	 * @param ElectionInterface<T> $election
	 * @return VoteInterface<T>
	 */
	public function vote(ElectionInterface $election) : VoteInterface;
	
}
