<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * BiasInterface interface file.
 * 
 * This interface represents processors that processes votes after they are
 * made and before they are counted by the voting method.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface BiasInterface extends Stringable
{
	
	/**
	 * Modifies the scores of the given vote by handling the citizen's data.
	 * 
	 * @param CitizenInterface<T> $citizen
	 * @param VoteInterface<T> $vote
	 * @return VoteInterface<T> the modified vote
	 */
	public function applyTo(CitizenInterface $citizen, VoteInterface $vote) : VoteInterface;
	
}
