<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * BiasFactoryInterface interface file.
 * 
 * This class specifies an object to create bias objects.
 * 
 * @author Anastaszor
 */
interface BiasFactoryInterface extends Stringable
{
	
	/**
	 * Creates a bias over boolean values.
	 * 
	 * @return BiasInterface<boolean>
	 */
	public function createBooleanBias() : BiasInterface;
	
	/**
	 * Creates a bias over integer values.
	 * 
	 * @return BiasInterface<integer>
	 */
	public function createIntegerBias() : BiasInterface;
	
	/**
	 * Creates a bias over float values.
	 * 
	 * @return BiasInterface<integer>
	 */
	public function createFloatBias() : BiasInterface;
	
	/**
	 * Creates a bias over string values.
	 * 
	 * @return BiasInterface<string>
	 */
	public function createStringBias() : BiasInterface;
	
}
