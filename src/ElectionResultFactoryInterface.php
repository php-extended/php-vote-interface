<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionResultFactoryInterface interface file.
 * 
 * This class specifies an object to create election result objects.
 * 
 * @author Anastaszor
 */
interface ElectionResultFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new election result to vote.
	 * 
	 * @template T of boolean|integer|float|string
	 * @param string $identifier
	 * @param array<integer, CandidateResultInterface<T>> $rankings
	 * @return ElectionResultInterface<T>
	 */
	public function createElectionResult(string $identifier, array $rankings) : ElectionResultInterface;
	
}
