<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * VoteInterface interface file.
 * 
 * This interface represents a ballot for a given citizen.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface VoteInterface extends Stringable
{
	
	/**
	 * The identifier of the vote given by the citizen.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the candidate rankings.
	 * 
	 * @return array<integer, CandidateRankingInterface<T>>
	 */
	public function getCandidateRanking() : array;
	
	/**
	 * Gets the overall score of this vote given by the citizen.
	 * 
	 * @return ScoreInterface
	 */
	public function getOverallScore() : ScoreInterface;
	
}
