<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use PhpExtended\Score\ScoreInterface;
use Stringable;

/**
 * CandidateResultInterface interface file.
 * 
 * This class represents the statistical results for a given candidate.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface CandidateResultInterface extends Stringable
{
	
	/**
	 * The id given by the election and voting method.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the candidate that is represented by this result.
	 * 
	 * @return CandidateInterface<T>
	 */
	public function getCandidate() : CandidateInterface;
	
	/**
	 * Gets the absolute number of points this candidate was given. In case of
	 * an election that does not manipulate points, this counts the number of 
	 * voices that this candidate gathered.
	 * 
	 * @return integer
	 */
	public function getPoints() : int;
	
	/**
	 * Gets the overall score of this candidate to be compared to other results.
	 * 
	 * @return ScoreInterface
	 */
	public function getScore() : ScoreInterface;
	
}
