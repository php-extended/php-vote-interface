<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionFactoryInterface interface file.
 * 
 * This class specifies an object to create election objects.
 * 
 * @author Anastaszor
 */
interface ElectionFactoryInterface extends Stringable
{
	
	/**
	 * Creates a Election over boolean values.
	 *
	 * @param string $identifier the identifier of the election
	 * @return ElectionInterface<boolean>
	 */
	public function createBooleanElection(string $identifier) : ElectionInterface;
	
	/**
	 * Creates a Election over integer values.
	 * 
	 * @param string $identifier the identifier of the election
	 * @return ElectionInterface<integer>
	 */
	public function createIntegerElection(string $identifier) : ElectionInterface;
	
	/**
	 * Creates a Election over float values.
	 * 
	 * @param string $identifier the identifier of the election
	 * @return ElectionInterface<integer>
	 */
	public function createFloatElection(string $identifier) : ElectionInterface;
	
	/**
	 * Creates a Election over string values.
	 * 
	 * @param string $identifier the identifier of the election
	 * @return ElectionInterface<string>
	 */
	public function createStringElection(string $identifier) : ElectionInterface;
	
}
