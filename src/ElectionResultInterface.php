<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * ElectionResultInterface interface file.
 * 
 * This class represents the 
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface ElectionResultInterface extends Stringable
{
	
	/**
	 * Gets the identifier of this election result, given by the election.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the iterator with all the results for the candidates after the
	 * election. This iterator is ordered by order of winning candidates after
	 * tiebreakers are applied (the first candidate result shows the first
	 * candidate, and so on until the last candidate is given).
	 * 
	 * @return array<integer, CandidateResultInterface<T>>
	 */
	public function getRankings() : array;
	
}
