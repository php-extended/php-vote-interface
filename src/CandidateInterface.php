<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-vote-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Vote;

use Stringable;

/**
 * CandidateInterface interface file.
 * 
 * This interface represents an object for which to vote to in an election.
 * 
 * @author Anastaszor
 * @template T of boolean|integer|float|string
 */
interface CandidateInterface extends Stringable
{
	
	/**
	 * Gets the id of the candidate given by the citizen.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the value that is given by this candidate.
	 * 
	 * @return ?T
	 */
	public function getValue();
	
}
